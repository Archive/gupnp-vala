/*
 * Copyright (C) 2008 OpenedHand Ltd.
 * Copyright (C) 2008,2010 Zeeshan Ali (Khattak) <zeeshanak@gnome.org>.
 *
 * Author: Jussi Kukkonen <jku@openedhand.com>
 *         Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *
 * This file is part of gupnp-vala.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

using GLib;
using GUPnP;

/*
 * TODO: 
 *  * make this example actually work ...
 *  
 *  * call setlocale
 *  * SIGTERM handler?
 */

public class Test.ServerTest : Object {
    Service content_dir;

    public static int main (string[] args) {
        Test.ServerTest test = new ServerTest ();
        return test.run (args);
    }

    private int run (string[] args) {
        Context ctxt;

        try {
            ctxt = new Context (null, null, 0);
        } catch (Error err) {
            critical (err.message);
            return 1;
        }

        print ("Running on port %u\n", ctxt.port);

        /* Create root device. Needs description.xml in working directory */
        RootDevice dev = new RootDevice (ctxt, "description.xml", ".");
        if (dev == null) {
            critical ("Creating root device failed");
            return 1;
        }

        /* Implement Browse action on ContentDirectory if available */
        content_dir =
            (Service) dev.get_service (
                    "urn:schemas-upnp-org:service:ContentDirectory");
        if (content_dir != null) {
            content_dir.action_invoked["Browse"].connect (on_browse);
            content_dir.notify_failed.connect (on_notify_failed);
            Timeout.add (5000, timeout);
        }

        dev.available = true;

        MainLoop loop = new MainLoop (null, false);
        loop.run();

        return 0;
    }

    private void on_browse (Service service, owned ServiceAction action) {
        print ("Browse action was invoked\n");

        print ("\tLocales: ");
        GLib.List<string> locales = action.get_locales ();
        foreach (string locale in locales)
            print ("%s,", locale);
        print ("\n");

        string filter;
        action.get ("Filter", typeof (string), out filter,
                    null);
        print ("\tFilter: %s\n", filter);

        action.set ("Result", typeof (string), "Hello world",
                    "NumberReturned", typeof (int), 0,
                    "TotalMatches", typeof (int), 0,
                    "UpdateID", typeof (int), 31415927,
                    null);

        action.return ();
    }

    private void on_notify_failed (Service service,
                                   List    callback_url,
                                   Error   reason) {
        print ("NOTIFY failed: %s\n", reason.message);
    }

    private bool timeout () {
        content_dir.notify ("SystemUpdateID",
                            typeof (uint),
                            27182818,
                            null);
        return false;
    }

}
