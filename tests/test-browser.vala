/*
 * Copyright (C) 2008 OpenedHand Ltd.
 * Copyright (C) 2008,2010 Zeeshan Ali (Khattak) <zeeshanak@gnome.org>.
 *
 * Author: Jussi Kukkonen <jku@openedhand.com>
 *         Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *
 * This file is part of gupnp-vala.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

using GLib;
using GSSDP;

public class TestBrowser : ResourceBrowser {

    private void on_resource_available (ResourceBrowser   browser,
                                        string            usn,
                                        GLib.List<string> locations) {
        GLib.print ("Resource available:\n  USN: %s\n", usn);
        for (weak List<string> l = locations; l != null; l = l.next) {
            GLib.print ("  Location: %s\n", l.data);
        }
    }

    private void on_resource_unavailable (ResourceBrowser browser,
                                          string      usn) {
        GLib.print ("Resource unavailable:\n  USN: %s\n", usn);
    }

    construct {
        this.resource_available.connect (this.on_resource_available);
        this.resource_unavailable.connect (this.on_resource_unavailable);
    }

    public TestBrowser() {
        Object (target: GSSDP.ALL_RESOURCES,
                client: new Client (null, null));
    }

    public static int main (string[] args) {
        TestBrowser browser = new TestBrowser ();

        browser.active = true;

        GLib.MainLoop loop = new MainLoop (null, false);
        loop.run();

        return 0;
    }

}

